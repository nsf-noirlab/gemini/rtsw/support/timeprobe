[schematic2]
uniq 1
[tools]
[detail]
s 704 384 100 0 Time info array
s 704 320 100 0 Bancom card present
s 48 384 100 0 Debug mode/flag
[cell use]
use bb200tr -544 -888 -100 0 frame
xform 0 736 -64
use egenSub 320 -409 100 0 egenSub#1
xform 0 464 16
p 400 352 100 0 1 FTA:LONG
p 400 288 100 0 1 FTVA:DOUBLE
p 400 256 100 0 1 FTVB:LONG
p 352 -480 100 0 1 INAM:initTime
p 416 272 100 0 0 NOA:1
p 416 240 100 0 0 NOB:1
p 400 208 100 0 1 NOVA:4
p 416 176 100 0 0 NOVB:1
p 352 -448 100 0 1 SCAN:Passive
p 352 -512 100 0 1 SNAM:getTime
p 432 -416 100 1024 -1 name:$(top)timeProbe:TAI
[comments]
